#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
Copyright (C) 2019  Michael Gehringer

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
'''

# genaral imports
from PyQt5.QtWidgets import (QWidget, QVBoxLayout, QHBoxLayout, QPushButton,
                            QLabel, QComboBox, QTextEdit, QSpacerItem,
                            QSizePolicy, QFileDialog)
from PyQt5.QtCore import QTimer
from PyQt5.QtGui import (QTextCursor)
import time

# local imports
from serialport import serialport

class MainWindow(QWidget):

    def __init__(self):
        super().__init__()
        self.left = 10
        self.top = 10
        self.width = 640
        self.height = 480
        self.initUI()

    def initUI(self):
        self.setWindowTitle("RS485 Viewer")
        self.setGeometry(self.left, self.top, self.width, self.height)
        vbox = QVBoxLayout()
        self.portWidget = PortChanger()
        vbox.addWidget(self.portWidget)
        self.term = TerminalArea()
        vbox.addWidget(self.term)
        footer = FooterWidget()

        vbox.addWidget(footer)
        # Create a QTimer
        self.timer = QTimer()
        # Connect it
        self.timer.timeout.connect(self.read_serialdata_terminal)

        # coonect pushbuttons
        self.portWidget.pbStart.clicked.connect(self.pbStart_pressed)
        self.portWidget.pbStop.clicked.connect(self.pbStop_pressed)
        footer.pbSave.clicked.connect(self.pbSave_pressed)
        footer.pbClear.clicked.connect(self.term.clear_terminal)

        self.setLayout(vbox)
        self.show()

    def pbStart_pressed(self):
        """
            This function is run when pbStart is pressed
        """

        if not self.timer.isActive():
            self.ser = serialport.SerialReader(
                            self.portWidget.get_selected_port())

            # Call every 1 seconds
            self.timer.start(1000)

    def pbStop_pressed(self):
        if self.timer.isActive():
            self.timer.stop()

    def pbSave_pressed(self):
        """
            save to csv. Open file dialog where to save CSV
        """
        # stop gathering data
        self.pbStop_pressed()
        filename, ending = QFileDialog.getSaveFileName(self, "Save file", "",
        ".csv")
        print(filename)
        if not(filename[-4:] == '.csv'):
            filename = filename + '.csv'

        with open(filename,'w') as f:
            f.writelines(self.term.get_lines())
        pass
    def read_serialdata_terminal(self):
        # create serial reader
        tmpTxt = self.ser.read()
        self.term.append_text(tmpTxt)


class PortChanger(QWidget):
    def __init__(self):
        super().__init__()
        hbox = QHBoxLayout()

        hbox.addWidget(QLabel('Port:'))

        self.cbPort = QComboBox()
        for i in serialport.serial_ports():
            self.cbPort.addItem(i)
        hbox.addWidget(self.cbPort)

        self.pbStart = QPushButton('Start')
        hbox.addWidget(self.pbStart)

        self.pbStop  = QPushButton('Stop')
        hbox.addWidget(self.pbStop)

        self.setLayout(hbox)

    def get_selected_port(self):
        """
            Return selcted port
        """
        return self.cbPort.currentText()

class TerminalArea(QWidget):

    def __init__(self):
        super().__init__()
        self.tfterminal = QTextEdit()
        self.tfterminal.setReadOnly(True)
        self.tfterminal.setLineWrapMode(QTextEdit.NoWrap)

        self.tfterminal.insertPlainText('Date;Time;Measuring' +
                                        'Value;Phase;Compensated Phase;' +
                                        'Temperature;Power Level;Current 1;' +
                                        'Current 2\n')

        hbox = QHBoxLayout()
        hbox.addWidget(self.tfterminal)

        self.setLayout(hbox)

    def append_text(self, text):
        """
            This function append the text to the textarea
        """
        self.tfterminal.moveCursor(QTextCursor.End)
        self.tfterminal.insertPlainText(text)
        self.tfterminal.moveCursor(QTextCursor.End)

    def clear_terminal(self):
        """
            clear data from terminal rewrite headerLine
        """
        self.tfterminal.clear()
        self.tfterminal.insertPlainText('Date;Time;Measuring' +
                                        'Value;Phase;Compensated Phase;' +
                                        'Temperature;Power Level;Current 1;' +
                                        'Current 2\n')
        self.tfterminal.moveCursor(QTextCursor.End)

    def get_lines(self):
        """
            returns string in terminal window
        """
        return self.tfterminal.toPlainText()

class FooterWidget(QWidget):

    def __init__(self):
        super().__init__()
        hbox = QHBoxLayout()
        hbox.addItem(QSpacerItem(20, 40,
                            QSizePolicy.Minimum,
                            QSizePolicy.Expanding))

        self.pbClear = QPushButton("clear")
        hbox.addWidget(self.pbClear)

        self.pbSave  = QPushButton("save")
        hbox.addWidget(self.pbSave)

        self.setLayout(hbox)
