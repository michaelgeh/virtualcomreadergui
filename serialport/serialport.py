#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Copyright (C) 2019  Michael Gehringer

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
"""


import sys
import glob
import serial

def serial_ports():
    """ Lists serial port names

        :raises EnvironmentError:
            On unsupported or unknown platforms
        :returns:
            A list of the serial ports available on the system
    """
    if sys.platform.startswith('win'):
        ports = ['COM%s' % (i + 1) for i in range(256)]
    elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
        # this excludes your current terminal "/dev/tty"
        ports = glob.glob('/dev/tty[A-Za-z]*')
    elif sys.platform.startswith('darwin'):
        ports = glob.glob('/dev/tty.*')
    elif sys.platform.startswith('freebsd'):
        ports = glob.glob('/dev/cuaU[0-9]')
    else:
        raise EnvironmentError('Unsupported platform')

    result = []
    for port in ports:
        try:
            s = serial.Serial(port)
            s.close()
            result.append(port)
        except (OSError, serial.SerialException):
            pass
    return result

class SerialReader(object):
    def __init__(self,port):
        self.port = port
        self.ser = serial.Serial(port,baudrate=115200)

    def read(self):
        """
            This function return the readed string.
            Return None when serial port is closed
        """
        if not self.ser.is_open:
            return None

        return self.ser.read_all().decode(errors='replace')

    def close_serial_port(self):
        """
            close serial port
        """
        self.ser.close()

    def reopen_serial_port(self):
        """
            reopen serial port
        """
        self.ser.open()

